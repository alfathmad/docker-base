# Entrypoint for docker
# Logging and supervisord
ln -sf /dev/stdout /var/log/nginx/access.log
ln -sf /dev/stderr /var/log/nginx/error.log
ln -sf /dev/stdout /var/log/php-fpm.log
supervisord -c /etc/supervisor/supervisord.conf
