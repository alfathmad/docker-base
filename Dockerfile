FROM ubuntu:22.04

LABEL maintainer="Muhammad Ayub Alfathoni <ayub@antikode.com>" \
      version.ubuntu="22.04" \
      version.mysql-client="8" \
      version.php="7.4" \
      version.composer="2.2.6" \
      version.nginx="1.22.0"

# Declare
ARG DEBIAN_FRONTEND=noninteractive
ARG php_version="7.4"
ARG composer_version="2.2.6"
ARG src_php="src/php"
ARG src_nginx="src/nginx"
ARG dst_nginx="/etc/nginx"
ARG src_spv="src/php"
ARG dst_spv="/etc/supervisor"

# Install package
RUN mkdir -p /root/.ssh; \
    apt update -y; \
    apt install --no-install-recommends software-properties-common apt-transport-https dirmngr -y; \
    apt install -y gpg-agent \
    gpg \
    gnupg; \
    apt-key export 991BC93C | gpg --dearmour -o /etc/apt/trusted.gpg.d/ubuntuarchive.gpg; \
    apt-key export E5267A6C | gpg --dearmour -o /etc/apt/trusted.gpg.d/ondrejppa.gpg; \
    add-apt-repository -y ppa:ondrej/php; \
    add-apt-repository -y ppa:ondrej/nginx; \
    apt update -y; \
    apt --no-install-recommends -y install \
    sudo \
    debconf-utils \
    curl \
    wget \
    supervisor \
    unzip \
    git \
    ssh \
    nano \
    net-tools \
    \
# Install php
    php${php_version} \
    php${php_version}-common \
    php${php_version}-cli \
    php${php_version}-fpm \
    php${php_version}-pdo \
    php${php_version}-xml \
    php${php_version}-dom \
    php${php_version}-mysql; \
    \
# Install nginx
    apt install nginx -y; \
    \
# Install composer
    cd /tmp; \
    curl -sS https://getcomposer.org/installer | php -- --version=${composer_version} --install-dir=/usr/local/bin --filename=composer; \
    \
# Install mysql client 8
    apt install mysql-client -y; \
    \
# Add user set UID and GID | make same as sysadmin user in NFS server
    useradd -ms /bin/bash app; \
    usermod -u 1000 app; \
    groupmod -g 1000 app;

# Config php
COPY ${src_php}/${php_version}/cli /etc/php/${php_version}/cli
COPY ${src_php}/${php_version}/fpm /etc/php/${php_version}/fpm

# Supervisor config
COPY ${src_spv}/${php_version}/supervisord.conf ${dst_spv}/supervisord.conf

# Nginx Module
RUN rm -rf /etc/nginx/conf.d/default.conf
COPY nginx-default.conf /etc/nginx/sites-enabled/default

# Set entrypoint
COPY start.sh /start.sh

# Cleanup build and cache
RUN apt -y autoremove; \
    apt -y autoclean; \
    rm -rf /var/lib/apt/lists/* /tmp/*;

# Set ownership and permision
RUN chown app:app -Rf /home/app; \
    chmod 775 /home/app; \
    chmod u+x /start.sh; \
    chown app:app -R /var/lib/php; \
    chmod 755 -R /var/lib/php;

ENTRYPOINT /start.sh

EXPOSE 80 9000